chrome.runtime.onInstalled.addListener(function() {
  var context = "image";
  var title = "Agregar Foto";
  var id = chrome.contextMenus.create({"title": title, "contexts":[context],
                                         "id": "context" + context});  
});

chrome.contextMenus.onClicked.addListener(onClickHandler);

//Evento Click
function onClickHandler(url, tab) {
    var storage = JSON.parse(localStorage.getItem('myurls')); 
    var myUrls= new Array();   
    
    //Verificamos que nuestro almacenamiento no este vacio (thruty js),
    //sino lo esta nos traemos las imagenes guardadas anteriormente y guardamos en myUrls
    if(storage)
      myUrls = storage;
                
    //Formateamos la url de la imagen al formato de foro [IMG][/IMG]
    var newurl = "[IMG]" + url.srcUrl + "[/IMG]\n";

    //La agregamos a nuestra lista de imagenes
    myUrls.push(newurl);
  
    //Guardamos en nuestro almacenamiento de localStorage HTML5              
    localStorage.setItem('myurls', JSON.stringify(myUrls));
    
    //Ejecutamos el comando setBadge de common.js 
    setBadge(myUrls.length.toString());

};
