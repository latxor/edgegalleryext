//Función encargada de copiar las fotos agregadas a través del browser_action.html - Botón Copiar
function copyGallery() {
  
  var storage = JSON.parse(localStorage.getItem('myurls'));   
  copyCommand(storage);

}

//Función encargada de borrar del localStorage las imagenes agregadas
function deleteGallery() {    
    localStorage.clear();
    setBadge("");    
}

document.getElementById('copybtn').addEventListener('click', copyGallery);
document.getElementById('deletebtn').addEventListener('click', deleteGallery);

$(document).ready(function() {    
    var clipboard = new Clipboard('.copyText');    
});