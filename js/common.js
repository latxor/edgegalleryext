//Función encargada de realizar el comando copiar
//Formateamos la cadena que se va a copiar en el clipboard y luego la copiamos
function copyCommand(ulrsArray)
{
    var result = "";
    ulrsArray.forEach(function(element) {
          result = result + element + '\n';
      }, this);
    document.getElementById('copybtn').setAttribute("data-clipboard-text", result);
}

//Función encargada de definir el numero de las images actualmente agregadas
function setBadge(stringBadge)
{
     chrome.browserAction.setBadgeText({text: stringBadge});
}